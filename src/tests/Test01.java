package tests;

/**
 * Questo codice mostra l'utilizzo dell'AND e OR logico in Java
 * @author giacomobellazzi
 *
 */
public class Test01 {

	public static void main(String[] args) {
		// Assegnazione delle variabili
		boolean b1 = false;
		boolean b2 = true;
		// AND logico
		boolean and = b1 && b2;
		// OR logico
		boolean or = b1 || b2;
		// Stampa a video con il metodo di concatenazione delle stringhe
		System.out.println("L'AND logico vale "+and);
		System.out.println("L'OR logico vale "+or);
	}

}
