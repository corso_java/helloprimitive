package tests;

/**
 * Questo programma effettua le operazioni base in Java
 * 
 * @author giacomobellazzi
 *
 */
public class Test02 {

	public static void main(String[] args) {
		int number1 = 30;
		int number2 = 20;
		// somma
		int sum = number1 + number2;
		// differenza
		int subtraction = number1 - number2;
		// moltiplicazione
		int mult = number1 * number2;
		// quoziente
		int quotient = number1 / number2;
		// resto
		int rest = number1 % number2;
		System.out.println("La somma di " + number1 + " con " + number2 + " vale " + sum);
		System.out.println(subtraction);
		System.out.println(mult);
		System.out.println(quotient);
		System.out.println(rest);
	}

}
