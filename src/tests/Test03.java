package tests;
/**
 * Questo programma mostra la differenza tra int e Integer
 * @author giacomobellazzi
 *
 */
public class Test03 {

	public static void main(String[] args) {
		int a = 10;
		value(a);
		System.out.println(a);
		Integer b = 10;
		// Integer b = new Integer(10);
		System.out.println(reference(b));
		System.out.println(b);
		b = null;
		System.out.println(b);
		Boolean isNull = (b == null);
		System.out.println(isNull);
	}

	public static void value(int value) {
		value = value + 1;
		// value++;
		// value+=1;
	}

	public static Integer reference(Integer value) {
		value = 5;
		value++;
		return value;
	}
}
